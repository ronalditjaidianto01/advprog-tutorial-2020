package id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.Armory;

public class SyntheticKnight extends Knight {

    public SyntheticKnight(Armory armory) {
        this.armory = armory;
        this.setName("Synthetic Knight");
    }

    @Override
    public void prepare() {
        // TODO complete me
        weapon = armory.craftWeapon();
        skill = armory.learnSkill();
    }
}
