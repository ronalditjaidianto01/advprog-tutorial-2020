package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import id.ac.ui.cs.advprog.tutorial4.singleton.core.HolyWish;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class HolyGrailTest {

    // TODO create tests
    @Mock
    private HolyWish holyWish;

    @InjectMocks
    private HolyGrail holyGrail;

    @Test
    public void whenMakeAWishIsCalledItShouldCallHolyWishSetWish() {
        String wish = "selesai";
        holyGrail.makeAWish(wish);

        verify(holyWish, times(1)).setWish(wish);
    }

    @Test
    public void whenGetHolyWishIsCalledItShouldReturnHolyWish() {
        HolyWish holyWish1 = holyWish.getInstance();

        HolyGrail grailSpy = spy(holyGrail);

        when(grailSpy.getHolyWish()).thenReturn(holyWish1);

        HolyWish calledHolyWish = grailSpy.getHolyWish();

        assertThat(calledHolyWish).isEqualTo(holyWish1);
    }
}
