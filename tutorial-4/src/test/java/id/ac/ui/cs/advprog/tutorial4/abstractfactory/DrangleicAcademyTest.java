package id.ac.ui.cs.advprog.tutorial4.abstractfactory;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.DrangleicAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.KnightAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MajesticKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.Knight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MetalClusterKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.SyntheticKnight;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class DrangleicAcademyTest {
    KnightAcademy drangleicAcademy;
    Knight majesticKnight;
    Knight metalClusterKnight;
    Knight syntheticKnight;

    @BeforeEach
    public void setUp() {
        // TODO setup me
        drangleicAcademy = new DrangleicAcademy();
        majesticKnight = drangleicAcademy.getKnight("majestic");
        metalClusterKnight = drangleicAcademy.getKnight("metal cluster");
        syntheticKnight = drangleicAcademy.getKnight("synthetic");
    }

    @Test
    public void checkKnightInstances() {
        // TODO create test
        assertTrue(majesticKnight instanceof MajesticKnight);
        assertTrue(metalClusterKnight instanceof MetalClusterKnight);
        assertTrue(syntheticKnight instanceof  SyntheticKnight);
    }

    @Test
    public void checkKnightNames() {
        // TODO create test
        assertEquals("Majestic Knight", majesticKnight.getName());
        assertEquals("Metal Cluster Knight", metalClusterKnight.getName());
        assertEquals("Synthetic Knight", syntheticKnight.getName());

        assertEquals("Metal Armor", majesticKnight.getArmor().getName());
        assertEquals("Thousand Jacker", majesticKnight.getWeapon().getName());
        assertEquals("Metal Armor", metalClusterKnight.getArmor().getName());
        assertEquals("Thousand Years of Pain", metalClusterKnight.getSkill().getName());
        assertEquals("Thousand Years of Pain", syntheticKnight.getSkill().getName());
        assertEquals("Thousand Jacker", syntheticKnight.getWeapon().getName());
    }

    @Test
    public void checkKnightDescriptions() {
        // TODO create test
        assertEquals("Metal-build armor", majesticKnight.getArmor().getDescription());
        assertEquals("Thousand jacker", majesticKnight.getWeapon().getDescription());
        assertEquals("Metal-build armor", metalClusterKnight.getArmor().getDescription());
        assertEquals("Thousand years of pain", metalClusterKnight.getSkill().getDescription());
        assertEquals("Thousand years of pain", syntheticKnight.getSkill().getDescription());
        assertEquals("Thousand jacker", syntheticKnight.getWeapon().getDescription());
    }

}
