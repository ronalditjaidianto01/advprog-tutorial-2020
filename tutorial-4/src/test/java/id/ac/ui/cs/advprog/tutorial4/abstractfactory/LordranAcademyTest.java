package id.ac.ui.cs.advprog.tutorial4.abstractfactory;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.DrangleicAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.LordranAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.KnightAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MajesticKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.Knight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MetalClusterKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.SyntheticKnight;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class LordranAcademyTest {
    KnightAcademy lordranAcademy;
    Knight majesticKnight;
    Knight metalClusterKnight;
    Knight syntheticKnight;

    @BeforeEach
    public void setUp() {
        // TODO setup me
        lordranAcademy = new LordranAcademy();
        majesticKnight = lordranAcademy.getKnight("majestic");
        metalClusterKnight = lordranAcademy.getKnight("metal cluster");
        syntheticKnight = lordranAcademy.getKnight("synthetic");
    }

    @Test
    public void checkKnightInstances() {
        // TODO create test
        assertTrue(majesticKnight instanceof MajesticKnight);
        assertTrue(metalClusterKnight instanceof MetalClusterKnight);
        assertTrue(syntheticKnight instanceof  SyntheticKnight);
    }

    @Test
    public void checkKnightNames() {
        // TODO create test
        assertEquals("Majestic Knight", majesticKnight.getName());
        assertEquals("Metal Cluster Knight", metalClusterKnight.getName());
        assertEquals("Synthetic Knight", syntheticKnight.getName());

        assertEquals("Shining Armor", majesticKnight.getArmor().getName());
        assertEquals("Shining Buster", majesticKnight.getWeapon().getName());
        assertEquals("Shining Armor", metalClusterKnight.getArmor().getName());
        assertEquals("Shining Force", metalClusterKnight.getSkill().getName());
        assertEquals("Shining Force", syntheticKnight.getSkill().getName());
        assertEquals("Shining Buster", syntheticKnight.getWeapon().getName());
    }

    @Test
    public void checkKnightDescriptions() {
        // TODO create test
        assertEquals("Shining shimmering armor", majesticKnight.getArmor().getDescription());
        assertEquals("Shining splendid buster", majesticKnight.getWeapon().getDescription());
        assertEquals("Shining shimmering armor", metalClusterKnight.getArmor().getDescription());
        assertEquals("Shining force b with u", metalClusterKnight.getSkill().getDescription());
        assertEquals("Shining force b with u", syntheticKnight.getSkill().getDescription());
        assertEquals("Shining splendid buster", syntheticKnight.getWeapon().getDescription());
    }
}
