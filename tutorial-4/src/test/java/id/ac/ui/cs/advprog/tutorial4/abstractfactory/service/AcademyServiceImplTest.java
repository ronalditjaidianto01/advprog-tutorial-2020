package id.ac.ui.cs.advprog.tutorial4.abstractfactory.service;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.KnightAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.LordranAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.LordranArmory;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.Knight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MajesticKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.repository.AcademyRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class AcademyServiceImplTest {

    @Mock
    private AcademyRepository academyRepository;

    @InjectMocks
    private AcademyServiceImpl academyService;

    // TODO create tests

    @Test
    public void whenProduceKnightisCalledItShouldCallAcademyRepositoryGetKnightAcademyByName() {
        AcademyService academySpy = spy(academyService);

        when(academyRepository.getKnightAcademyByName("Lordran")).thenReturn(new LordranAcademy());
        academySpy.produceKnight("Lordran", "majestic");
        verify(academyRepository, times(1)).getKnightAcademyByName("Lordran");
    }

    @Test
    public void whenProduceKnightisCalledKnightAttributeShouldNotBeNull() {
        AcademyService academySpy = spy(academyService);

        when(academyRepository.getKnightAcademyByName("Lordran")).thenReturn(new LordranAcademy());
        academySpy.produceKnight("Lordran", "majestic");

        Knight knight = academyRepository.getKnightAcademyByName("Lordran").getKnight("majestic");
        when(academySpy.getKnight()).thenReturn(knight);

        Knight calledKnight = academySpy.getKnight();
        assertThat(calledKnight).isEqualTo(knight);
    }

    @Test
    public void whenGetKnightAcademiesisCalledItShouldReturnListOfAcademy(){
        List<KnightAcademy> academyList = new ArrayList<>();
        academyList.add(new LordranAcademy());

        AcademyService academySpy = spy(academyService);

        when(academySpy.getKnightAcademies()).thenReturn(academyList);

        Iterable<KnightAcademy> calledKnightAcademy = academySpy.getKnightAcademies();

        assertThat(calledKnightAcademy).isEqualTo(academyList);
    }

    @Test
    public void whenGetKnightIsCalledItShouldReturnKnight(){
        Knight knight = null;

        AcademyService academySpy = spy(academyService);

        when(academySpy.getKnight()).thenReturn(knight);

        Knight calledKnight = academySpy.getKnight();

        assertThat(calledKnight).isEqualTo(knight);
    }
}
