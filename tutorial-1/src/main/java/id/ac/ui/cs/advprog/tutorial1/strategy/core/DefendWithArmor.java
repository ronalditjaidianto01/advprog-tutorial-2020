package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithArmor implements DefenseBehavior {
        //ToDo: Complete me
    @Override
    public String getType() {
        return "Armor";
    }

    @Override
    public String defend() {
        return "You can never hurt me with this armor!";
    }
}
