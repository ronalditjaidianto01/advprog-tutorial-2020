package id.ac.ui.cs.advprog.tutorial5.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SoulTest {
    private Soul soul;

    @BeforeEach
    public void setUp() {
        soul = new Soul();
        soul.setAge(1);
        soul.setGender("M");
        soul.setName("Haha");
        soul.setOccupation("Commedian");
    }

    @Test
    public void testGetterMethodAge() {
        assertEquals(1,soul.getAge());
    }

    @Test
    public void testGetterMethodName() {
        assertEquals("Haha",soul.getName());
    }

    @Test
    public void testGetterMethodId() {
        assertEquals(0,soul.getId());
    }

    @Test
    public void testGetterMethodOccupation() {
        assertEquals("Commedian",soul.getOccupation());
    }

    @Test
    public void testGetterMethodGender() {
        assertEquals("M",soul.getGender());
    }
}
